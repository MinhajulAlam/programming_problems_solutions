//11172 - Relational Operator
#include<stdio.h>
int main()
{
    int n, i, num1, num2;

    scanf("%d",&n);

    for(i=0;i<n;i++){
        scanf("%d%d",&num1,&num2);

        if(num1>num2)
            printf(">\n");
        else if(num1<num2)
            printf("<\n");
        else if(num1==num2)
            printf("=\n");
    }

    return 0;
}
