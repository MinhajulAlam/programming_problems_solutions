//5.Write a program to create an array of n elements and then delete an element from the list.

#include<stdio.h>
int main()
{
    int array[100], n, i, position;
    int in_arr[100];

    printf("How many numbers?\n");
    scanf("%d",&n);
    printf("Enter %d numbers:\n",n);

    //Input numbers in array
    for(i=1;i<=n;i++){
        scanf("%d",&array[i]);
    }
    //storing numbers in another array
    for(i=1;i<=n;i++){
        in_arr[i]=array[i];
    }


    printf("In which position you want to delete numbers?\n");
    scanf("%d",&position);


    //deleting numbers in array
    for(i=position;i<=n-1;i++){
        array[i]=in_arr[i+1];
    }


    //output
    printf("\n\n");
    printf("New Array:\n");
    for(i=1;i<=n-1;i++){
        printf("%d ",array[i]);
    }

    return 0;
}
