//1.Write a program to find the largest number from a given list of integers.

#include<stdio.h>
int main()
{
    int store[100],i, n, j, max;

    printf("How many numbers?\n");
    scanf("%d",&n);
    printf("Enter %d numbers:\n",n);

    //input
    for(i=1;i<=n;i++){
        scanf("%d",&store[i]);
    }

    //finding the largest number
    max=store[1];
    for(i=2;i<=n;i++){
        if(max<store[i]){
            max=store[i];
        }


    }
    printf("Largest integers number is %d\n",max);

    return 0;
}
