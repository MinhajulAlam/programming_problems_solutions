/*3.Write a program to create an array of n elements to read the marks of n students
and then count how many students passed [pass marks>=40] in the examination.*/

#include<stdio.h>
int main()
{
    int student[100],n, i, pass=0;

    printf("How many students?\n");
    scanf("%d",&n);
    printf("Enter the marks of %d students: \n",n);

    //input marks of students
    for(i=1;i<=n;i++){
        scanf("%d",&student[i]);
    }

    //checking for passed students
    for(i=1;i<=n;i++)
    {
        if(student[i]>=40){
            pass++;
        }
    }

    //output
    printf("Number of passed students are %d\n",pass);

    return 0;
}
