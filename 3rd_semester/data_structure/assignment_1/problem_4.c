//4.Write a program to create an array of n elements and then insert an element to the list.

#include<stdio.h>
int main()
{
    int array[100],n,i, new_number,position;
    int in_arr[100];

    printf("How many numbers?\n");
    scanf("%d",&n);
    printf("Enter %d numbers:\n",n);

    //Input numbers in array
    for(i=1;i<=n;i++){
        scanf("%d",&array[i]);
    }
    //storing numbers in another array
    for(i=1;i<=n;i++){
        in_arr[i]=array[i];
    }

    printf("Enter a number to insert in the array\n");
    scanf("%d",&new_number);

    printf("In which position you want to insert %d numbers?\n");
    scanf("%d",&position);


    //inserting numbers in array
    array[position]=new_number;
    for(i=position+1;i<=n+1;i++){
        array[i]=in_arr[i-1];
    }


    //output
    printf("\n\n");
    printf("New Array:\n");
    for(i=1;i<=n+1;i++){
        printf("%d ",array[i]);
    }

    return 0;
}
