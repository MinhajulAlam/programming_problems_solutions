//6.Write a program to sort n numbers using Bubble Sort algorithm.

#include<stdio.h>
int main()
{
    int i, j, n, array[100], temp;

    printf("How many numbers?\n");
    scanf("%d",&n);
    printf("Enter %d numbers\n",n);

    //input numbers
    for(i=0;i<n;i++){
        scanf("%d",&array[i]);
    }

    //sorting
    for(i=0;i<n;i++){
        for(j=i+1;j<n;j++){
            if(array[i]>array[j]){
                temp=array[j];
                array[j]=array[i];
                array[i]=temp;
            }
        }
    }

    //output
    for(i=0;i<n;i++){
        printf("%d ",array[i]);
    }

    return 0;
}
