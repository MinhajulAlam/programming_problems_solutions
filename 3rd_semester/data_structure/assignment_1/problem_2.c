//2.Write a program to calculate the roots of the quadratic equation ax^2+bx+c=0; where a, b and c are known.

#include<stdio.h>
#include<math.h>
int main()
{
    double a, b, c, m;
    double x1,x2;

    printf("Enter 3 value:\n");
    scanf("%lf%lf%lf",&a,&b,&c);


    m=(b*b)-(4*a*c);
    x1=(-b+sqrt(m))/(2*a);
    x2=(-b-sqrt(m))/(2*a);

    printf("Roots are %f \t%f",x1,x2);

    return 0;
}
